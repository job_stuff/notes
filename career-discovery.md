# Tips from rwxRob on finding what you want to do in a job. 

* Start by listing what you like to do everyday. If not that, figure out what you don't like. 

* Write them down. Ponder on it. Pray on it. 

* Don't limit it to technology. 

* Figure out which careers/jobs allows you to do those things you enjoy. 

* Associate the sheet of tasks with jobs that fit. And get the titles. 

* Next, make a list of employers that will pay you for those titles. 

* You are looking for what jobs that exists that fit the bill. You are also looking for an ecosystem. 

* Make sure the list of companies is small. 

* Next, reach out to the companies and look for someone that does the job that you want to do.

* Ask, what one skill that would make it so you had to hire me, what would it be? 


_Building a skill stack_ 

* Make a document with three columns. Skill name, Level of knowledge of skill, Level you need to be to get a job. 

* List the skills you need to learn for a given job. 

* 0 is no knowledge, 1 is you know what it is, 2 you can do somethings/know enough to be dangerous, 3 know enough to be employed, 4 Subject matter expert on a team, 5 Knowledgeable to give talks/teach/presentations. 


