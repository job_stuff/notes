# Skill needed for being a Tech Artist/Director 

**Name**    **Current Skill**   **Desired Skill** 
C++         0                   5 
Python/APIs 0                   3 
Maya        2                   5 
Houdini     0                   4
Git         2                   3 
Mantra      0                   3 
V-Ray       0                   3 
Kinematics  0                   3 
Hair/Cloth simulation 0         3
